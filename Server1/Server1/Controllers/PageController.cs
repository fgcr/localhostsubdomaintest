﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Routing;

namespace Server1.Controllers
{
    [ApiController]
    public class PageController : ControllerBase
    {
        [Route("{Page=Home}")]
        [HttpGet]
        public ActionResult Home()
        {
            return File("index.html", "text/html");
        }
    }
}
