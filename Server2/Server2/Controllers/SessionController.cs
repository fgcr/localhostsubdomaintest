﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Server2.Controllers
{
    [ApiController]
    [Route("[Controller]")]
    public class SessionController : ControllerBase
    {
        [HttpGet("get")]
        public JsonResult GetSession()
        {
            var value = HttpContext.Session.GetString("SessionKey");

            return new JsonResult(value);
        }

        [HttpPost("set")]
        public JsonResult SetSession([FromBody] string sessionValue)
        {
            HttpContext.Session.SetString("SessionKey", sessionValue);

            return new JsonResult(true);
        }
    }
}
