﻿using Microsoft.AspNetCore.Mvc;

namespace Server2.Controllers
{
    [ApiController]
    public class PageController : ControllerBase
    {
        [Route("{Page=Home}")]
        [HttpGet]
        public ActionResult Home()
        {
            return File("index.html", "text/html");
        }
    }
}
