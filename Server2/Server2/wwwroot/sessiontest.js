﻿document.getElementById("SetSessionValueButton").addEventListener('click', () => {
    fetch("/session/set",
    {
        method: "post",
        headers : {
            'Content-Type' : 'application/json'
        },
        body: JSON.stringify(document.getElementById("SetSessionValue").value)
    });
});

document.getElementById("GetSessionValueButton").addEventListener('click', () => {
    async function GetSessionValue() {
        const result = await fetch("/session/get",
            {
                method: "get",
                headers: {
                    'Content-Type': 'application/json'
                }
            });

        const resultJson = await result.json();

        console.info(resultJson);

        document.getElementById("GetSessionValue").innerText = resultJson;
    }

    GetSessionValue();
});